#ifndef NPC_H_
#define NPC_H_

#include "SDL2Common.h"
#include "Sprite.h"
#include "AABB.h"

// Forward declerations
// improve compile time. 
class Vector2f;
class Animation;
class Game;

class NPC : public Sprite
{
private:
    
    // Animation state
    int state;
        
    // Sprite information
    static const int SPRITE_HEIGHT = 64;
    static const int SPRITE_WIDTH = 32;

    // Need game
    Game* game;

    // weapon range
    float maxRange;
    float timeToTarget;
    int health;


public:
    NPC();
    ~NPC();

    // Player Animation states
    enum NPCState{LEFT=0, RIGHT, UP, DOWN, IDLE};
    
    void init(SDL_Renderer *renderer);
    void update(float timeDeltaInSeconds);

    // Update 'things' ai related
    
    void ai();
    void setGame(Game* game);

    int getCurrentAnimationState();

    void takeDamage(int damage);
    bool isDead();
    void respawn(const int MAX_HEIGHT, const int MAX_WIDTH);
    enum NPCState { LEFT = 0, RIGHT, UP, DOWN, IDLE, DEAD };

};

#endif